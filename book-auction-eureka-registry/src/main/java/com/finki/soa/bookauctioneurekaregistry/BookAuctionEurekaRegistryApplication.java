package com.finki.soa.bookauctioneurekaregistry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class BookAuctionEurekaRegistryApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookAuctionEurekaRegistryApplication.class, args);
	}
}
