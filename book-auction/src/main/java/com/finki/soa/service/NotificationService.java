package com.finki.soa.service;

import com.finki.soa.model.Book;
import com.finki.soa.model.Notification;
import com.finki.soa.model.Person;
import com.finki.soa.repository.NotificationRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NotificationService {

	@Autowired
	private NotificationRepository notificationRepository;

	public Iterable<Notification> findAll() {
		return notificationRepository.findAll();
	}

	public Iterable<Notification> notifyUsersWhoLost(Book book, List<Person> users) {
		users.forEach(user -> {
			Notification notification = new Notification(book, user, false);
			notificationRepository.save(notification);
		});
		return this.findAll();
	}

	public void notifyWinner(Book book, Person user) {
		Notification notification = new Notification(book, user, true);
		notificationRepository.save(notification);
	}

	public List<Notification> findLosers(Book book) {
		return notificationRepository.findNotificationsByBookAndSuccess(book, false);
	}

	public Notification findWinner(Book book) {
		return notificationRepository.findNotificationsByBookAndSuccess(book, true).get(0);
	}

}
