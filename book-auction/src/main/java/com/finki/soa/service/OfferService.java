package com.finki.soa.service;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.finki.soa.model.Book;
import com.finki.soa.model.Offer;
import com.finki.soa.model.Person;
import com.finki.soa.repository.OfferRepository;

@Service
public class OfferService {

	@Autowired
	private OfferRepository offerRepository;

	public Iterable<Offer> saveOffer(Book book, Person user, Integer amount) {
		Offer offer = new Offer(book, user, amount);
		offerRepository.save(offer);
		return this.findAll();
	}

	public Iterable<Offer> findAll() {
		return offerRepository.findAll();
	}

	public List<Offer> findAllByBook(Book book) {
		return offerRepository.findAllByBook(book);
	}

	public Set<Book> findAllBooksUsedInOffers() {
		return offerRepository.findAllBooksUsedInOffers();
	}
}
