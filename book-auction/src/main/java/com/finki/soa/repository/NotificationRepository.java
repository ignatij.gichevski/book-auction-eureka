package com.finki.soa.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.finki.soa.model.Book;
import com.finki.soa.model.Notification;

public interface NotificationRepository extends CrudRepository<Notification, Long>{

	List<Notification> findNotificationsByBookAndSuccess(Book book, boolean b);

}
