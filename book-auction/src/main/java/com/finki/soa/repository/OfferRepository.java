package com.finki.soa.repository;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import com.finki.soa.model.Book;
import com.finki.soa.model.Offer;

public interface OfferRepository extends CrudRepository<Offer, Long> {

	List<Offer> findAllByBook(Book book);

	default Set<Book> findAllBooksUsedInOffers() {
		Set<Book> books = new HashSet<>();
		findAll().forEach(offer -> {
			books.add(offer.getBook());
		});
		return books;
	}

}
