Build the project and the docker images:
mvn package docker:build

Go to docker-compose script:
cd src/main/docker

Start containers defined in docker-compose:
docker-compose up -d

Ensure containers are running:
docker ps

If not all containers are running (for instance on first run the spring application would fail because the database is initializing) run the containers again:
docker-compose up -d

Go to Traefik dashboard: http://0.0.0.0:8080

Scale application with 3 instances:
docker-compose scale my-app=3

Downscale application with 1 instance:
docker-compose scale my-app=1

Shutdown containers:
docker-compose down
